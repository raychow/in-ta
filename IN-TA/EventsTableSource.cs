﻿using System;
using System.Collections.Generic;
using System.IO;
using Foundation;
using UIKit;
using EventKit;
using EventKitUI;

namespace INTA {
	public class EventsTableSource : UITableViewSource {

		protected EKEvent[] tableItems;
		protected string cellIdentifier = "TableCell";
		UIViewController owner;

		public EventsTableSource (EKEvent[] items, UIViewController owner)
		{
			tableItems = items;
			this.owner = owner;

		}
			
		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}
			
		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return tableItems.Length;
		}
			
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			//get event detail using event identifier
			EKEvent mySavedEvent = App.Current.EventStore.EventFromIdentifier ( tableItems [indexPath.Row].EventIdentifier );
			//create an event view controller to see event's detail
			var eventDetailViewController = new EKEventViewController ();
			eventDetailViewController.Event = mySavedEvent;
			//navigation bar setting
			UINavigationController navController = new UINavigationController (eventDetailViewController);
			//setting delegate for controlling bar button item
			var eventControllerDelegate = new EventDetialViewDelegate ( eventDetailViewController );
			eventDetailViewController.Delegate = eventControllerDelegate;
			//present view controller using modal
			owner.PresentModalViewController (navController, true);

			tableView.DeselectRow (indexPath, true);
		}

		protected class EventDetialViewDelegate : EventKitUI.EKEventViewDelegate
		{
			// we need to keep a reference to the controller so we can dismiss it
			protected EventKitUI.EKEventViewController eventController;

			public EventDetialViewDelegate (EventKitUI.EKEventViewController eventController)
			{
				//left bar button item for adding people from yammer
				UIBarButtonItem btnAdd = new UIBarButtonItem();
				btnAdd.Title = "+ People";
				btnAdd.Clicked += BtnAdd_Clicked;
				// save our controller reference
				this.eventController = eventController;
				this.eventController.NavigationItem.SetLeftBarButtonItem(btnAdd,false);

			}

			void BtnAdd_Clicked (object sender, EventArgs e)
			{
				this.eventController.NavigationController.PushViewController
				(new AddPeopleToEventViewController(this.eventController.Event),true);
			}
				
			// completed is called when a user eith
			public override void Completed (EventKitUI.EKEventViewController controller, EKEventViewAction action)
			{
				eventController.DismissModalViewController (true);
			}
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (cellIdentifier);
			string item = tableItems[indexPath.Row].Title;

			//---- if there are no cells to reuse, create a new one
			if (cell == null)
			{ cell = new UITableViewCell (UITableViewCellStyle.Default, cellIdentifier); }

			cell.TextLabel.Text = item;


			return cell;
		}
			
	}
}
