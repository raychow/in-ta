﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.MvvmCross.ViewModels;
using Foundation;
using UIKit;
using Cirrious.MvvmCross.Plugins.Sqlite;
using INTA.Data;
using System.Linq;
using System;
using Microsoft.WindowsAzure.MobileServices;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using System.Collections.Generic;
using EventKit;

namespace INTA
{
	
	[Register ("AppDelegate")]
	public class AppDelegate : MvxApplicationDelegate
	{
		private ISQLiteConnectionFactory factory;
		private ISQLiteConnection connection;



		// class-level declarations
		public override UIWindow Window {
			get;
			set;
		}

		public override void FinishedLaunching (UIApplication application)
		{
			var setup = new Setup (this, Window);
			setup.Initialize ();
            CurrentPlatform.Init();
            CreateDatabase ();
		
		}
			


		public void CreateDatabase(){

			//connection with local database
			factory = Mvx.Resolve<ISQLiteConnectionFactory> ();
			// open or create the database
			connection = factory.Create ("inta.sql");
			connection.CreateTable<Friends> ();
			connection.CreateTable<Profile> ();

			var cursor = connection.Query<Profile> ("Select * from Profile");
			if (cursor.Count () <= 0) {
				var person = new Profile {
					FirstName = "First Name",
					LastName = "Last Name",
					DateOfBirth = DateTime.Today,
					Email = "Please login with your yammer account",
					ProfileIcon = null,
					AccessToken = null,
					Position = "no title",
					userid = 0
				};
				connection.Insert (person);
				connection.Close ();
			}	
		}

		public override void OnResignActivation (UIApplication application)
		{
			// Invoked when the application is about to move from active to inactive state.
			// This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
			// or when the user quits the application and it begins the transition to the background state.
			// Games should use this method to pause the game.
		}

		public override void DidEnterBackground (UIApplication application)
		{
			// Use this method to release shared resources, save user data, invalidate timers and store the application state.
			// If your application supports background exection this method is called instead of WillTerminate when the user quits.
		}

		public override void WillEnterForeground (UIApplication application)
		{
			// Called as part of the transiton from background to active state.
			// Here you can undo many of the changes made on entering the background.
		}

		public override void OnActivated (UIApplication application)
		{
			// Restart any tasks that were paused (or not yet started) while the application was inactive. 
			// If the application was previously in the background, optionally refresh the user interface.
		}

		public override void WillTerminate (UIApplication application)
		{
			// Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
		}
	}
}