﻿using System;
using Foundation;
using UIKit;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using INTA.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Xamarin.Auth;
using CoreGraphics;
using Cirrious.MvvmCross.ViewModels;
using INTA.Data.ViewModels;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using Microsoft.Practices.ServiceLocation;
using AddressBookUI;
using AddressBook;
using MessageUI;

namespace INTA
{
	public partial class PeopleDetailViewController : MvxViewController
	{
		//accesstoken for get user details
		private string _accessToken;
		//loading view
		private bool openingLoading;
		private LoadingOverlay loadingOverlay;
		//define Viewmodel for value and function
		public new PeopleDetailViewModel ViewModel { get { return (PeopleDetailViewModel)base.ViewModel; } }
		//local value for storing userid
		private int _userID;
		//local value for handling image icon
		private string _imgUrl;
		private byte[] _imgArray;
		//declare ABPerson
		ABNewPersonViewController _newPersonController;
		//bool for is in the list or not
		private bool _isFriend;
		//declare email view controller
		MFMailComposeViewController mailController;
		//declare yammer auth
		OAuthConnector yammerConnector;

		public PeopleDetailViewController (int userID, bool isFriend) : base ("PeopleDetailViewController", null)
		{
			//title
			Title = "Details";
			//userid
			_userID = userID;
			//is friend?
			_isFriend = isFriend;
			//hide the bottom bar
			this.HidesBottomBarWhenPushed = true;
			//initialise ABNewPersonViewController
			_newPersonController = new ABNewPersonViewController ();
			//ABNewPersonViewController delegate setting
			var personControllerDelegate = new PersonViewDelegate ( _newPersonController,this );
			_newPersonController.Delegate = personControllerDelegate;

		}

		//save contact when user clicked add to list or contact
		public void saveContact(){
			ViewModel.saveContact(_imgArray);
			var okAlert = new UIAlertView ();
			okAlert.Title = "Message";
			okAlert.Message = "Successfully added";
			okAlert.AddButton ("OK");
			okAlert.Show ();
			_isFriend = true;
			btnAddToList.Hidden = true;
		}

		//handle add button event
		void BtnAddToList_TouchUpInside (object sender, EventArgs e)
		{
			saveContact ();
		}

		//delegate handle person view from ABNewPersonViewController
		protected class PersonViewDelegate : ABNewPersonViewControllerDelegate
		{
			// we need to keep a reference to the controller so we can dismiss it
			protected ABNewPersonViewController _newPersonViewController;
			protected PeopleDetailViewController _parentController;

			public PersonViewDelegate (ABNewPersonViewController eventController, PeopleDetailViewController parentController)
			{

				// save our controller reference
				this._newPersonViewController = eventController;
				this._parentController = parentController;
			}

			// completed is called when a user eith
			public override void  DidCompleteWithNewPerson(ABNewPersonViewController controller, ABPerson person)
			{
				_newPersonViewController.DismissModalViewController (true);
				if (person != null) {
					_parentController.saveContact ();
				}
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			//request mvvm cross
			this.Request = new MvxViewModelRequest<PeopleDetailViewModel> (null, null, new MvxRequestedBy ());
			base.ViewDidLoad ();
			//mvvm data binding
			var set = this.CreateBindingSet<PeopleDetailViewController, Data.ViewModels.PeopleDetailViewModel>();

			set.Bind (lblFirstName).For("Text").To (vm => vm.FirstName);
			set.Bind (lblLastName).For("Text").To (vm => vm.LastName);
			set.Bind (lblPosition).For("Text").To (vm => vm.JobTitle);
			set.Bind (lblEmail).For ("Text").To (vm => vm.Email);
			set.Bind (lblBirthday).For ("Text").To (vm => vm.Birthday);
			set.Bind (txtViewAbout).For ("Text").To (vm => vm.About);
			set.Apply ();

			//bar button item for user to send email
			UIBarButtonItem btnEmail = new UIBarButtonItem ();
			btnEmail.Title = "Send Email";
			this.NavigationItem.SetRightBarButtonItem (btnEmail, true);
			btnEmail.Clicked += BtnEmail_Clicked;
			//handling for button Add to Contact and Add to List 
			btnAddToContact.TouchUpInside += BtnAddToContact_TouchUpInside;
			btnAddToList.TouchUpInside += BtnAddToList_TouchUpInside;
			//initialise yammer connector
			yammerConnector = new OAuthConnector (this);
			//display loading view
			displayLoading ();
		}
		//handle button add to contact event
		void BtnAddToContact_TouchUpInside (object sender, EventArgs e)
		{
			//ABMutableMultiValue for storing email of a new person who will be added
			ABMutableMultiValue<string> email = new ABMutableStringMultiValue();
			email.Add(new NSString(lblEmail.Text), new NSString("Work"));
			//local value for stroing person
			var person = new ABPerson ();
			person.FirstName = lblFirstName.Text;
			person.LastName = lblLastName.Text;
			person.JobTitle = lblPosition.Text;
			person.Image = NSData.FromUrl (new NSUrl (_imgUrl));
			person.SetEmails (email);
			//person displays in ABnewPersonviewcontroller
			_newPersonController.DisplayedPerson = person;
			//add navgiation controller to ABnewPersonviewcontroller
			UINavigationController navController = new UINavigationController (_newPersonController);
			//present ABnewPersonviewcontroller
			this.PresentModalViewController (navController,
				true);

		}
		//handling button Email event
		void BtnEmail_Clicked (object sender, EventArgs e)
		{
			
			if (MFMailComposeViewController.CanSendMail) {
				mailController = new MFMailComposeViewController ();
				mailController.SetToRecipients (new string[]{ lblEmail.Text });
				this.PresentViewController (mailController, true, null);

				mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
					Console.WriteLine (args.Result.ToString ());
					args.Controller.DismissViewController (true, null);
				};
			} else {
				UIAlertView emailAlert = new UIAlertView ();
				emailAlert.Title = "Cannot Send Email";
				emailAlert.Message = "Cannot send email because you have not set any email on this devices";
				emailAlert.AddButton("OK");
				emailAlert.Show ();
			}
		}

		//set Icon image using the value from mvvm
		public void setIconImage(){
			//set icon image
			if (!string.IsNullOrEmpty(_imgUrl)) {

				NSData imgData = NSData.FromUrl(new NSUrl(_imgUrl));
				_imgArray = imgData.ToArray ();
				UIImage img = new UIImage (NSData.FromArray(_imgArray));
				imgView.Image = img;
			}
		}


		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			displayLoading ();
			checkAccessToken ();
			if (_isFriend == true) {
				btnAddToList.Hidden = true;
			} else {
				btnAddToList.Hidden = false;
			}

		}
			
		//check if it has accesstoken , get required user details
		//otherwise show login dialog
		private void checkAccessToken(){
			if (yammerConnector.GetAccessToken() != null) {
				_accessToken = yammerConnector.GetAccessToken ();
				GetDetailsFromUserID ();
				loadingOverlay.Hide ();
				openingLoading = false;
			} else {
				loadingOverlay.Hide ();
				openingLoading = false;
				showDialogLogin ();
			}
		}

		//show login dialog
		public void showDialogLogin(){
			var loginAlert = UIAlertController.Create("Yammer Login Required", "To see people details, please login to yammer.", UIAlertControllerStyle.Alert);
			loginAlert.AddAction (UIAlertAction.Create ("Cancel", UIAlertActionStyle.Default, null));
			loginAlert.AddAction (UIAlertAction.Create ("Login", UIAlertActionStyle.Default, (action)=> connectToYammer()));
			this.PresentViewController(loginAlert,true,null);
		}

		//open web view for yammer auth
		private void connectToYammer(){
			yammerConnector.startAuthentication ();
		}

		//display loading view
		public void displayLoading(){
			if (openingLoading == false) {
				var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
				if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
					bounds.Size = new CGSize (bounds.Size.Height, bounds.Size.Width);
				}
				// show the loading overlay on the UI thread using the correct orientation sizing
				this.loadingOverlay = new LoadingOverlay (bounds);
				this.View.Add (this.loadingOverlay);
				openingLoading = true;
			}
		}

		//get details via mvvm
		private void GetDetailsFromUserID(){
			if (_userID != 0 && _accessToken != null) {
				ViewModel.PeopleUserID = _userID;
				ViewModel.AccessToken = _accessToken;
				ViewModel.DoRefresh ();
				ViewModel.GetPeopleDetailFromYammer ();
				loadingOverlay.Hide ();
				openingLoading = false;
				_imgUrl = ViewModel.getImgUrl ();
				setIconImage ();
			} else {
				loadingOverlay.Hide ();
				openingLoading = false;
			}
		}
	}
}

