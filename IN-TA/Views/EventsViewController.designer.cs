// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace INTA
{
	[Register ("EventsViewController")]
	partial class EventsViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView EventsTable { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIBarButtonItem SettingsBarBtn { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (EventsTable != null) {
				EventsTable.Dispose ();
				EventsTable = null;
			}
			if (SettingsBarBtn != null) {
				SettingsBarBtn.Dispose ();
				SettingsBarBtn = null;
			}
		}
	}
}
