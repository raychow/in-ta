// This file has been autogenerated from a class added in the UI designer.
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;
using System;
using INTA.Data.ViewModels;
using Cirrious.MvvmCross.ViewModels;
using INTA.Data;
using Factorymind.Components;
using EventKit;
using System.Collections.Generic;
using System.Linq;



namespace INTA
{
	public partial class CalendarViewController : MvxViewController
	{
		//declare ekcalendar
		EKCalendar calendar;
		//Ekcalendar array for storing ekcalendar
		EKCalendar[] calendarArray;
		//Event Array
		EKEvent[] eventsArray;
		//Declare the UI components
		UILabel label;
		UITableView tableView;
		FMCalendar fmCalendar;

		public CalendarViewController (IntPtr handle) : base (handle)
		{
			Title = NSBundle.MainBundle.LocalizedString ("Calendar", "Calendar");
		}

		public override void ViewDidLoad ()
		{
			//mvvm cross request
			this.Request = new MvxViewModelRequest<CalendarViewModel>(null, null, new MvxRequestedBy());
			base.ViewDidLoad();
			//initialise UI components
			label = new UILabel(new CGRect(0, 420, View.Frame.Width, 18));
			tableView = new UITableView (new CGRect ( 0, 445, View.Frame.Width, 200));
			tableView.ScrollEnabled = true;
			//mvvm cross data binding
			var set = this.CreateBindingSet<CalendarViewController, Data.ViewModels.CalendarViewModel>();
			set.Bind(label).For("Text").To(vm => vm.Calendar);
			set.Apply();



			// Inititalise scroll view
			scrollView = new UIScrollView (
				new CGRect (0, 0, View.Frame.Width, View.Frame.Height));
			scrollView.ScrollEnabled = true;
			scrollView.ContentSize = new CGSize (320, 720);
			scrollView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			View.AddSubview (scrollView);

		}
		bool AccessGanted;
		public override void ViewDidAppear(bool animated)
		{
			//request access to user's calendar
				App.Current.EventStore.RequestAccess (EKEntityType.Event, 
					(bool granted, NSError e) => {
					if (granted){
						CheckAndCreateCustomCalendar();
						AccessGanted = true;
					}
						else
							new UIAlertView ( "Access Denied", 
								"User Denied Access to Calendar Data", null,
								"ok", null).Show ();
					} );

			//Initialise Calendar View
			fmCalendar = new FMCalendar (new CGRect (0, 0, View.Frame.Width, View.Frame.Height-180));

			View.BackgroundColor = UIColor.White;

			// Specify selection color
			fmCalendar.SelectionColor = UIColor.Red;

			// Specify today circle Color
			fmCalendar.TodayCircleColor = UIColor.Red;

			// Customizing appearance
			fmCalendar.LeftArrow = UIImage.FromFile ("leftArrow.png");
			fmCalendar.RightArrow = UIImage.FromFile ("rightArrow.png");

			fmCalendar.MonthFormatString = "MMMM yyyy";

			// Shows Sunday as last day of the week
			fmCalendar.SundayFirst = false;

			if (AccessGanted == true) {
				//Get IN-TA Calendar
				GetCalendar ();
				//Get Event from IN-TA Calendar
				GetEventsViaQuery ();

				//Get Today Event from IN-TA Calendar
				GetEventOnThatDate (DateTime.Today);

				// Mark with a dot dates that fulfill the predicate
				fmCalendar.IsDayMarkedDelegate = (date) => {
					bool dateHasEvent = false;
					foreach (var singleEvent in eventsArray) {
						var eventDate = (DateTime)singleEvent.StartDate;
						if (eventDate.Date.Equals (date.Date)) {
							dateHasEvent = true;
							break;
						}
					}
					return dateHasEvent == true;
				};

				// Turn gray dates that fulfill the predicate
				fmCalendar.IsDateAvailable = (date) => {
					return (date >= DateTime.Today);
				};

				fmCalendar.MonthChanged = (date) => {
				};

				fmCalendar.DateSelected += (date) => {
					label.Text = "Events On " + date.Date.ToString ("dd MMM yyyy");
					GetEventOnThatDate (date);
				};

				//check UI Components is exist in the view or not
				if (fmCalendar.IsDescendantOfView (scrollView)) {
					scrollView.AddSubview (fmCalendar);
					scrollView.AddSubview (label);
					scrollView.AddSubview (tableView);
				} else {
					//Exist, remove and add them again
					fmCalendar.RemoveFromSuperview ();
					label.RemoveFromSuperview ();
					tableView.RemoveFromSuperview ();
					scrollView.AddSubview (fmCalendar);
					scrollView.AddSubview (label);
					scrollView.AddSubview (tableView);
				}
				label.Text = "Events On Today";
			}
		}

		//get IN-TA Calendar
		public void GetCalendar(){
			var calendars = App.Current.EventStore.Calendars;
			foreach (var c in calendars) {
				if(c.Title.Equals("IN-TA")){
					calendar = c;
					calendarArray = new EKCalendar[]{ c };
					break;
				}
			}
		}
		private bool calendarCreated;
		public void CheckAndCreateCustomCalendar(){
			//check if the calendar exist or not
			var calendarlist = App.Current.EventStore.Calendars;

			foreach (var c in calendarlist) {
				if(c.Title.Equals("IN-TA")){
					calendarCreated = true;
					break;
				}
			}
			//if not exist, create one
			if (calendarCreated == false) {
				EKCalendar calendar = EKCalendar.Create (EKEntityType.Event, App.Current.EventStore);
				calendar.Title = "IN-TA";
				EKSource theSource = null;
				foreach (EKSource source in App.Current.EventStore.Sources) {
					if (source.SourceType == EKSourceType.CalDav) {
						theSource = source;
						break;
					}
				}
				calendar.Source = theSource;
				NSError error = null;
				App.Current.EventStore.SaveCalendar (calendar, true, out error);
			}
		}


		//Get Event based on theDate value
		protected void GetEventOnThatDate(DateTime thatDate){
			List<EKEvent> _eventOnThatDate;
			_eventOnThatDate = new List<EKEvent> ();
			foreach(var singleEvent in eventsArray){
				var eventDate = (DateTime)singleEvent.StartDate;
				if (eventDate.Date.Equals(thatDate.Date)) {
					_eventOnThatDate.Add (singleEvent);
				}
			}
			var tableSource = new EventsTableSource (_eventOnThatDate.ToArray (), this);
			tableView.Source = tableSource;
			tableView.ReloadData ();
		}

		//Get Events From the Calendar
		protected void GetEventsViaQuery ()
		{
			// create our NSPredicate which we'll use for the query
			var startDate = (NSDate)DateTime.Now;
			var endDate = (NSDate)DateTime.Now.AddMonths (6);

			// the third parameter is calendars we want to look in, to use all calendars, we pass null
			NSPredicate query = App.Current.EventStore.PredicateForEvents (startDate, endDate, calendarArray);
			// execute the query
			eventsArray = App.Current.EventStore.EventsMatching (query);
		
		}
	}
}
