// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace INTA
{
	[Register ("ProfileViewController")]
	partial class ProfileViewController
	{
		[Outlet]
		UIKit.UIBarButtonItem btnEdit { get; set; }

		[Outlet]
		UIKit.UIBarButtonItem btnSettings { get; set; }

		[Outlet]
		UIKit.UIButton btnYammer { get; set; }

		[Outlet]
		UIKit.UIButton imgIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblPosition { get; set; }

		[Outlet]
		UIKit.UIScrollView scrollView { get; set; }

		[Outlet]
		UIKit.UITextField txtDateOfBirth { get; set; }

		[Outlet]
		UIKit.UITextField txtEmail { get; set; }

		[Outlet]
		UIKit.UITextField txtFirstName { get; set; }

		[Outlet]
		UIKit.UITextField txtLastName { get; set; }

		[Outlet]
		UIKit.UITextView txtViewAbout { get; set; }

		[Action ("btnEdit_Activated:")]
		partial void btnEdit_Activated (UIKit.UIBarButtonItem sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnEdit != null) {
				btnEdit.Dispose ();
				btnEdit = null;
			}

			if (btnSettings != null) {
				btnSettings.Dispose ();
				btnSettings = null;
			}

			if (btnYammer != null) {
				btnYammer.Dispose ();
				btnYammer = null;
			}

			if (imgIcon != null) {
				imgIcon.Dispose ();
				imgIcon = null;
			}

			if (lblPosition != null) {
				lblPosition.Dispose ();
				lblPosition = null;
			}

			if (scrollView != null) {
				scrollView.Dispose ();
				scrollView = null;
			}

			if (txtDateOfBirth != null) {
				txtDateOfBirth.Dispose ();
				txtDateOfBirth = null;
			}

			if (txtEmail != null) {
				txtEmail.Dispose ();
				txtEmail = null;
			}

			if (txtFirstName != null) {
				txtFirstName.Dispose ();
				txtFirstName = null;
			}

			if (txtLastName != null) {
				txtLastName.Dispose ();
				txtLastName = null;
			}

			if (txtViewAbout != null) {
				txtViewAbout.Dispose ();
				txtViewAbout = null;
			}
		}
	}
}
