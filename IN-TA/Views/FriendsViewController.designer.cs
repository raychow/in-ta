// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace INTA
{
	[Register ("FriendsViewController")]
	partial class FriendsViewController
	{
		[Outlet]
		UIKit.UIBarButtonItem btnAdd { get; set; }

		[Outlet]
		UIKit.UIButton btnLogin { get; set; }

		[Outlet]
		UIKit.UILabel label { get; set; }

		[Outlet]
		UIKit.UITableView TableView { get; set; }

		[Outlet]
		UIKit.UISearchBar UISearchBar { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnLogin != null) {
				btnLogin.Dispose ();
				btnLogin = null;
			}

			if (label != null) {
				label.Dispose ();
				label = null;
			}

			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}

			if (btnAdd != null) {
				btnAdd.Dispose ();
				btnAdd = null;
			}

			if (UISearchBar != null) {
				UISearchBar.Dispose ();
				UISearchBar = null;
			}
		}
	}
}
