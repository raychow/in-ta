﻿
using System;

using Foundation;
using UIKit;
using EventKit;
using Cirrious.MvvmCross.Touch.Views;
using INTA.Data.ViewModels;
using Cirrious.MvvmCross.ViewModels;
using INTA.Data;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using CoreGraphics;
using ObjCRuntime;
using System.IO;
using MessageUI;
using Xamarin.Auth;

namespace INTA
{
	public partial class AddPeopleToEventViewController : MvxViewController
	{
		//Declare value
		//local value for EKevent
		EKEvent _event;
		//Define Viewmodel for value changes and view model
		public new AddPeopleToEventViewModel ViewModel { get { return (AddPeopleToEventViewModel)base.ViewModel; } }
		//Declare handling loadiing view
		LoadingOverlay loadingOverlay;
		bool openingLoading = false;
		//bar button for invite
		UIBarButtonItem btnInvite;
		//declare mail view controller from ios
		MFMailComposeViewController mailController;
		//declare yammer connector
		OAuthConnector yammerConnector;

		public AddPeopleToEventViewController (EKEvent Event) : base ("AddPeopleToEventViewController", null)
		{
			_event = Event;
			Title = "Invite People";
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			//hide button bar
			this.HidesBottomBarWhenPushed = true;

			//Add invite button to nav bar
			btnInvite = new UIBarButtonItem ();
			btnInvite.Title = "Invite";
			btnInvite.Clicked += BtnInvite_Clicked;
			this.NavigationItem.SetRightBarButtonItem (btnInvite, true);

			//initialise yammer connector
			yammerConnector = new OAuthConnector (this);

			//request mvvm cross
			this.Request = new MvxViewModelRequest<AddPeopleToEventViewModel>(null, null, new MvxRequestedBy());
			base.ViewDidLoad();

			//handle searchbar on text change
			searchBar.TextChanged += SearchBar_TextChanged;
			//mvvm binding
			var set = this.CreateBindingSet<AddPeopleToEventViewController, Data.ViewModels.AddPeopleToEventViewModel>();
			set.Apply();
		}
		//declare list of string to store email
		List<string> inviteesEmail;

		//button invite clicked event
		void BtnInvite_Clicked (object sender, EventArgs e)
		{
			//initialise list of email
			inviteesEmail = new List<string>();
			//get indexpaths from user's selection on tableview
			var indexpaths = tableView.IndexPathsForSelectedRows;
			//if user selects at one cell
			if (indexpaths != null) {
				foreach (var index in indexpaths) {
					var cell = tableView.CellAt (index);
					var email = cell.DetailTextLabel.Text;
					inviteesEmail.Add (email);
				}
				CreateAnInvitationEmail (inviteesEmail.ToArray ());
			} else {
				//if user hasn't select any person
				UIAlertView selectAlert = new UIAlertView ();
				selectAlert.Title = "No Person is Selected";
				selectAlert.Message = "Please select people who you want to invite.";
				selectAlert.AddButton("OK");
				selectAlert.Show ();
			}
		}

		//create a email with attachment - ics event calendar file
		private void CreateAnInvitationEmail(string[] emails)
		{
			//getting from _event and some details of user
			string schLocation = _event.Location;
			string schSubject = _event.Title;
			string schDescription = _event.Notes;
			DateTime schBeginDate = (DateTime)_event.StartDate;
			DateTime schEndDate = (DateTime)_event.EndDate;
			ViewModel.getUserContact ();
			string organizer = ViewModel.FirstName + " " + ViewModel.LastName;
			string orgEmail = ViewModel.Email;

			//string array to store ics format for the event
			String[] contents = { "BEGIN:VCALENDAR",
				"PRODID:-//Flo Inc.//FloSoft//EN",
				"BEGIN:VEVENT",
				"DTSTART:" + schBeginDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"),
				"DTEND:" + schEndDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"),
				"LOCATION:" + schLocation,
				"ORGANIZER;CN="+ organizer + ":MAILTO:" + orgEmail,
				"DESCRIPTION;ENCODING=QUOTED-PRINTABLE:" + schDescription,
				"SUMMARY:" + schSubject,
				"END:VEVENT",
				"END:VCALENDAR" };

			//get folder path inside the app
			var documents = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);

			//get filepath
			string filePath = documents + "/" + _event.Title + ".ics";
			//save ics file inside the app
			System.IO.File.WriteAllLines(filePath, contents);

			//check user's email in this device
			//if user has set
			if (MFMailComposeViewController.CanSendMail) {
				mailController = new MFMailComposeViewController ();
				mailController.SetToRecipients (emails);
				var file = NSData.FromFile (filePath);
				mailController.AddAttachmentData(file, "text/calendar", _event.Title +".ics");

				this.PresentViewController (mailController, true, null);

				mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
					Console.WriteLine (args.Result.ToString ());
					args.Controller.DismissViewController (true, null);
				};
			} else {

				//if user has not set
				UIAlertView emailAlert = new UIAlertView ();
				emailAlert.Title = "Cannot Send Email";
				emailAlert.Message = "Cannot send email because you have not set any email on this devices";
				emailAlert.AddButton("OK");
				emailAlert.Show ();
			}

		}
			
		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			displayLoading ();
			RefreshFriendsList ();
			if (yammerConnector.checkAccessToken () != true) {
				btnInvite.Enabled = false;
				showDialogLogin ();
			} else {
				ViewModel.DoRefreshData ();
				btnInvite.Enabled = true;
			}
		}

		//open webview for yammer auth
		private void connectToYammer(){
			yammerConnector.startAuthentication();
		}

		//dialog message for login
		public void showDialogLogin(){
			var loginAlert = UIAlertController.Create("Yammer Login Required", "To add people from your yammer network.", UIAlertControllerStyle.Alert);
			loginAlert.AddAction (UIAlertAction.Create ("Cancel", UIAlertActionStyle.Default, null));
			loginAlert.AddAction (UIAlertAction.Create ("Login", UIAlertActionStyle.Default, (action)=> connectToYammer()));
			this.PresentViewController(loginAlert,true,null);
		}

		//handle search bar text changed event
		void SearchBar_TextChanged (object sender, UISearchBarTextChangedEventArgs e)
		{
			if (searchBar.Text.Length != 0) {
				var _friendsList = ViewModel.Friends;
				var _searchItem = new List<Friends> ();
				foreach (var f in _friendsList) {
					if (f.FullName.Contains (searchBar.Text)) {
						_searchItem.Add (f);
					}
					var source = new AddFriendsTableSource (_searchItem.ToArray (), this);
					tableView.Source = source;
					tableView.ReloadData ();
				}
			} else {
				RefreshFriendsList ();
			}
		}

		//display loading view
		public void displayLoading(){
			if (openingLoading == false) {
				var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
				if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
					bounds.Size = new CGSize (bounds.Size.Height, bounds.Size.Width);
				}
				// show the loading overlay on the UI thread using the correct orientation sizing
				this.loadingOverlay = new LoadingOverlay (bounds);
				this.View.Add (this.loadingOverlay);
				openingLoading = true;
			}
		}

		//refresh friends from mvvm cross
		private void RefreshFriendsList (){
			ViewModel.DoRefreshData ();
			var _friendsList = ViewModel.Friends;
			var source = new AddFriendsTableSource (_friendsList.ToArray (), this);
			tableView.Source = source;
			tableView.ReloadData ();
			loadingOverlay.Hide ();
			openingLoading = false;
		}
	}
}

