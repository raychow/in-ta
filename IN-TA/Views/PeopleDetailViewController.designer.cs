// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace INTA
{
	[Register ("PeopleDetailViewController")]
	partial class PeopleDetailViewController
	{
		[Outlet]
		UIKit.UIButton btnAddToContact { get; set; }

		[Outlet]
		UIKit.UIButton btnAddToList { get; set; }

		[Outlet]
		UIKit.UIImageView imgView { get; set; }

		[Outlet]
		UIKit.UILabel lblBirthday { get; set; }

		[Outlet]
		UIKit.UILabel lblEmail { get; set; }

		[Outlet]
		UIKit.UILabel lblFirstName { get; set; }

		[Outlet]
		UIKit.UILabel lblLastName { get; set; }

		[Outlet]
		UIKit.UILabel lblPosition { get; set; }

		[Outlet]
		UIKit.UITextView txtViewAbout { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAddToContact != null) {
				btnAddToContact.Dispose ();
				btnAddToContact = null;
			}

			if (btnAddToList != null) {
				btnAddToList.Dispose ();
				btnAddToList = null;
			}

			if (imgView != null) {
				imgView.Dispose ();
				imgView = null;
			}

			if (lblBirthday != null) {
				lblBirthday.Dispose ();
				lblBirthday = null;
			}

			if (lblEmail != null) {
				lblEmail.Dispose ();
				lblEmail = null;
			}

			if (lblFirstName != null) {
				lblFirstName.Dispose ();
				lblFirstName = null;
			}

			if (lblLastName != null) {
				lblLastName.Dispose ();
				lblLastName = null;
			}

			if (lblPosition != null) {
				lblPosition.Dispose ();
				lblPosition = null;
			}

			if (txtViewAbout != null) {
				txtViewAbout.Dispose ();
				txtViewAbout = null;
			}
		}
	}
}
