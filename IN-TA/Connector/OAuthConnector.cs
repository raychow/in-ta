﻿using System;
using Newtonsoft.Json.Linq;
using Xamarin.Auth;
using UIKit;
using Foundation;
using System.Net;
using System.IO;
using System.Threading;

namespace INTA.Data
{
	public class OAuthConnector
	{
		OAuth2Authenticator auth;
		UIViewController _viewController;
		INTADatabase intaDatabase;
		public ManualResetEvent mrse = new ManualResetEvent(false);
		string _firstName;
		string _lastName;
		string _email;
		DateTime _dateOfBirth;
		string _profileIcon;
		string _about;
		string _position;
		int _userID;
		string _accessToken;

		public bool checkAccessToken(){
			if (_accessToken != null) {
				return true;
			} else {
				return false;
			}
		}

		public string GetAccessToken(){
			return _accessToken;
		}

		public void SetAccessToken(string token){
			_accessToken = token;
			intaDatabase.setAccessToken (token);
		}

		public OAuthConnector(UIViewController vc){

			_viewController = vc;
			intaDatabase = new INTADatabase ();
			intaDatabase.getProfile ();
			_accessToken = intaDatabase.GetAccessToken ();
		 auth = new OAuth2Authenticator (
				clientId: "ynpdcGVLo1U61XCluHCBQ",//yammer
				scope: "",
				authorizeUrl: new Uri ("https://www.yammer.com/dialog/oauth"),
				redirectUrl: new Uri ("https://c15chow.au.auth0.com/mobile"));//auth0 redirect url
		
	}

		public void startAuthentication(){
			UIViewController vc = auth.GetUI ();
			_viewController.PresentViewController (vc, true, null);


			auth.Completed += (s, ee) => {
				if(ee.IsAuthenticated != false){
				var values = ee.Account.Properties;
				var access_token = values["access_token"].ToString();
				UpdateUser(access_token);
				}
				_viewController.DismissViewController(true,null);
			};
		}

		public void UpdateUser(string token)
		{
			intaDatabase.getProfile ();
			SetAccessToken (token);
			GetInformation (token);
			intaDatabase.UpdateProfile (_firstName, _lastName, _email, _dateOfBirth, _profileIcon, _about,  _position, _userID);

		}

		public void GetInformation(string _accessToken){

			var url = "https://www.yammer.com/api/v1/users/current.json";


			HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create (url);
			httpWebRequest.Method = "GET";
			httpWebRequest.Headers ["Authorization"] =
				"Bearer " + _accessToken;
			httpWebRequest.Accept = "application/json";
			httpWebRequest.BeginGetResponse (Response_Completed, httpWebRequest);
			mrse.WaitOne ();
		}

		private void Response_Completed(IAsyncResult result)
		{
			HttpWebRequest request = (HttpWebRequest)result.AsyncState;
			HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);

			using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
			{
				string responseFromServer = streamReader.ReadToEnd();

				JObject user = JObject.Parse (responseFromServer);

				_profileIcon = user ["mugshot_url"].ToString ();
				_userID = (int)user ["id"];

				if(!String.IsNullOrEmpty(user["first_name"].ToString()))
					_firstName = user["first_name"].ToString();
				if(!String.IsNullOrEmpty(user["last_name"].ToString()))
					_lastName = user["last_name"].ToString();
				if(!String.IsNullOrEmpty(user["email"].ToString()))
					_email = user ["email"].ToString();
				if(!String.IsNullOrEmpty(user["birth_date"].ToString()))
					_dateOfBirth = Convert.ToDateTime(user ["birth_date"].ToString());

				if (!String.IsNullOrEmpty(user ["job_title"].ToString())) {
					var position = user ["job_title"].ToString();
					if (!String.IsNullOrEmpty(user ["location"].ToString())) {
						var location = user ["location"].ToString ();
						var fullTitle = position + " At " + location;
						_position = fullTitle;
					} else {
						_position = position;
					}
				}

				if (!String.IsNullOrEmpty (user ["interests"].ToString ())) {
					_about = "Interests:" + user ["interests"].ToString ();
				} else {
					_about = "Interests: No Data";
				}

				if (!String.IsNullOrEmpty (user ["summary"].ToString ())) {
					_about += "\n\nAbout:" + user ["summary"].ToString ();
				} else {
					_about += "\n\nAbout: No Data";
				}
			}
			mrse.Set ();
		}
	}
}

