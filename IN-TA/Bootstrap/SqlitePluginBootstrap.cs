using Cirrious.CrossCore.Plugins;

namespace INTA.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Sqlite.PluginLoader, Cirrious.MvvmCross.Plugins.Sqlite.Touch.Plugin>
    {
    }
}