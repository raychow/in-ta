using Cirrious.CrossCore.Plugins;

namespace INTA.Bootstrap
{
    public class FilePluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.File.PluginLoader, Cirrious.MvvmCross.Plugins.File.Touch.Plugin>
    {
    }
}