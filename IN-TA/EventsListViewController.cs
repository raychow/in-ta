﻿using System;
using System.Linq;
using System.Collections.Generic;
using UIKit;
using MonoTouch.Dialog;
using Foundation;
using EventKit;

namespace INTA
{
	public class EventListViewController : UITableViewController
	{
		protected EKEvent[] events;
		//declare ekcalendar
		EKCalendar calendar;
		//Ekcalendar array for storing ekcalendar
		EKCalendar[] calendarArray;

		public EventListViewController ()
		{
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad();
			//request access to user's calendar
			App.Current.EventStore.RequestAccess (EKEntityType.Event, 
				(bool granted, NSError e) => {
					if (granted){
						CheckAndCreateCustomCalendar();
						GetCalendar ();
					}else
						new UIAlertView ( "Access Denied", 
							"User Denied Access to Calendar Data", null,
							"ok", null).Show ();
				} );

		}

		private bool calendarCreated;
		public void CheckAndCreateCustomCalendar(){
			//check if the calendar exist or not
			var calendarlist = App.Current.EventStore.Calendars;

			foreach (var c in calendarlist) {
				if(c.Title.Equals("IN-TA")){
					calendarCreated = true;
					break;
				}
			}
			//if not exist, create one
			if (calendarCreated == false) {
				EKCalendar calendar = EKCalendar.Create (EKEntityType.Event, App.Current.EventStore);
				calendar.Title = "IN-TA";
				EKSource theSource = null;
				foreach (EKSource source in App.Current.EventStore.Sources) {
					if (source.SourceType == EKSourceType.CalDav) {
						theSource = source;
						break;
					}
				}
				calendar.Source = theSource;
				NSError error = null;
				App.Current.EventStore.SaveCalendar (calendar, true, out error);
			}
		}

		//get the calendar "IN-TA"
		public void GetCalendar(){
			var calendars = App.Current.EventStore.Calendars;
			foreach (var c in calendars) {
				if(c.Title.Equals("IN-TA")){
					calendar = c;
					calendarArray = new EKCalendar[]{ c };
					break;
				}
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			GetEventsViaQuery ();
		}

		protected void GetEventsViaQuery ()
		{
			// create our NSPredicate which we'll use for the query
			var startDate = (NSDate)DateTime.Now;
			var endDate = (NSDate)DateTime.Now.AddMonths (6);

			// the third parameter is calendars we want to look in, to use all calendars, we pass null
			NSPredicate query = App.Current.EventStore.PredicateForEvents (startDate, endDate, calendarArray);
			// execute the query
			events = App.Current.EventStore.EventsMatching (query);
			if (this.events != null) {
				var tableSource = new EventsTableSource (this.events, this);
				TableView.Source = tableSource;
				TableView.ReloadData ();
			}
		}
	}
}


