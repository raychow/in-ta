﻿using System;
using UIKit;
using INTA.Data;
using Foundation;
using System.Drawing;

namespace INTA
{
	public class FriendsTableSource: UITableViewSource
	{
		UIViewController  _peopleViewController;
		Friends[] tableItems;
		private static readonly NSString FriendsCellIdentifier = new NSString("FriendsCell");
		UIButton btnInfo;

		public FriendsTableSource (Friends[] items, UIViewController peopleViewController)
		{
			tableItems = items;
			_peopleViewController = peopleViewController;
		}


		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return tableItems.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (FriendsCellIdentifier);

			//---- if there are no cells to reuse, create a new one
			if (cell == null)
			{ cell = new UITableViewCell (UITableViewCellStyle.Subtitle, FriendsCellIdentifier); }

			cell.TextLabel.Text = tableItems[indexPath.Row].FullName;
			cell.TextLabel.TextAlignment = UITextAlignment.Left;


			cell.ImageView.Image = UIImage.LoadFromData(NSData.FromArray(tableItems[indexPath.Row].ImgUrl));
//			cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;

			return cell;
		}

		public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			var _userID = tableItems [indexPath.Row].UserId;
			_peopleViewController.NavigationController.PushViewController (new PeopleDetailViewController (_userID, true), true);
			tableView.DeselectRow (indexPath, true);

		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var _userID = tableItems [indexPath.Row].UserId;
			_peopleViewController.NavigationController.PushViewController (new PeopleDetailViewController (_userID, true), true);
			tableView.DeselectRow (indexPath, true);
		}

		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			return true; // return false if you wish to disable editing for a specific indexPath or for all rows
		}
	}
}

