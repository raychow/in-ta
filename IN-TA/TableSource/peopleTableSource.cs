﻿using System;
using Cirrious.MvvmCross.Binding.Touch.Views;
using UIKit;
using Foundation;
using INTA.Data;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using CoreGraphics;

namespace INTA
{
	public class peopleTableSource: UITableViewSource
	{
		private static readonly NSString PeopleCellIdentifier = new NSString("PeopleCell");
		People[] tableItems;
		UIViewController  _peopleViewController;
		INTADatabase intaDatabase;

		public peopleTableSource (People[] items, UIViewController peopleViewController)
		{
			tableItems = items;
			_peopleViewController = peopleViewController;
			intaDatabase = new INTADatabase ();
			intaDatabase.getFriends ();
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return tableItems.Length;
		}


		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (PeopleCellIdentifier);

			//---- if there are no cells to reuse, create a new one
			if (cell == null)
			{ cell = new UITableViewCell (UITableViewCellStyle.Subtitle, PeopleCellIdentifier); }

			cell.TextLabel.Text = tableItems[indexPath.Row].Name;
			cell.TextLabel.TextAlignment = UITextAlignment.Left;
			//			cell.DetailTextLabel.Text = tableItems [indexPath.Row].Position;
			//			cell.DetailTextLabel.TextAlignment = UITextAlignment.Left;
			//			cell.ImageView.Image = UIImage.LoadFromData(NSData.FromUrl(new NSUrl(tableItems[indexPath.Row].ImgUrl)));

			cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;

			return cell;
		}

		public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			var _userID = tableItems [indexPath.Row].UserId;
			var _friendList = intaDatabase.getFriendsList ();
			var checkIsFriend = _friendList.SingleOrDefault(r => r.UserId == _userID);
			if (checkIsFriend != null) {
				_peopleViewController.NavigationController.PushViewController (new PeopleDetailViewController (_userID, true), true);
			} else {
				_peopleViewController.NavigationController.PushViewController (new PeopleDetailViewController (_userID, false), true);
			}
				tableView.DeselectRow (indexPath, true);
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var _userID = tableItems [indexPath.Row].UserId;
			var _friendList = intaDatabase.getFriendsList ();
			var checkIsFriend = _friendList.SingleOrDefault(r => r.UserId == _userID);
			if (checkIsFriend != null) {
				_peopleViewController.NavigationController.PushViewController (new PeopleDetailViewController (_userID, true), true);
			} else {
				_peopleViewController.NavigationController.PushViewController (new PeopleDetailViewController (_userID, false), true);
			}
			tableView.DeselectRow (indexPath, true);
		}
	}
}

