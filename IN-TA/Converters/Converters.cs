﻿using System;
using Cirrious.CrossCore.Converters;
using UIKit;
using Foundation;
using System.IO;

namespace INTA
{
	public class Converters
	{
		public class MvxInMemoryImageValueConverter : MvxValueConverter<byte[], UIImage>
		{
			protected override UIImage Convert(byte[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
			{
				if (value == null)
					return null;

				var imageData = NSData.FromArray(value);
				return UIImage.LoadFromData(imageData);
			}

//			protected override byte[] ConvertBack(UIImage value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
//			{
//				if (value == null)
//					return null;
//				NSData imageData = value.AsPNG ();
//				Byte[] pByteArray = new Byte[imageData.Length];
//				System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, pByteArray, 0, Int32.Parse(pByteArray));
//
//				return pByteArray;
//			}
//
//			protected int ConvertToInt(object o){
//				
//				return 0;
//			}
		}
	}
}

