﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace INTA.Data
{
	public class People
	{
		public int UserId { get; set; }
		public string Name{ get; set; }
		public string Position{ get; set; }
		public string ImgUrl{ get; set; }

		public People (int user_id, string name){
			UserId = user_id;
			Name = name;
		}
	}
}

