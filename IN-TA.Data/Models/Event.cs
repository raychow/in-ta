﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Sqlite;

namespace INTA.Data
{
	public class Event
	{
        public string id { get; set; }
        public int EventID{ get; set; }
		public bool Private{ get; set; }
		public string EventName{ get; set; }
		public string Description{ get; set; }
		public DateTime Start{ get; set; }
		public DateTime End{ get; set; }
		//location
		public string Location{get;set;}
		public double Lon{ get; set; }
		public double Lat{ get; set; }
		//userid
		public int Owner { get; set; }
		public string Joined{ get; set; }

	}
}

