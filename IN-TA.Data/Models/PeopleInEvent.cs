﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace INTA.Data
{
	public class PeopleInEvent
	{
		public int UserId { get; set; }
		public string Name{ get; set; }
	}
}
