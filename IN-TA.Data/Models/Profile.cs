﻿using System;
using Cirrious.MvvmCross.Plugins.Sqlite;


namespace INTA.Data
{
	public class Profile
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public int userid{ get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime DateOfBirth{ get; set; }

		public string Email{ get; set; }

		public string ProfileIcon{ get; set; }

		public string About{ get; set; }

		public string Position{ get; set; }

		public string AccessToken{ get; set; }

	}
}

