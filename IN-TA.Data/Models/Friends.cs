﻿using System;
using Cirrious.MvvmCross.Plugins.Sqlite;

namespace INTA.Data
{
	public class Friends
	{
		[PrimaryKey]
		public int UserId{ get; set;}

		public string FullName{ get; set; }

		public string JobTitle{ get; set; }

		public byte[] ImgUrl{ get; set; }

		public string Email{ get; set; }
	}
}