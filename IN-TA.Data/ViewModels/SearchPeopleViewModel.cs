﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Threading;

//Actually this mvvm is unused
namespace INTA.Data.ViewModels
{
	public class SearchPeopleViewModel:MvxViewModel
	{
		private string accessToken;
		private INTADatabase profiledb;
		public ManualResetEvent mrse = new ManualResetEvent(false);



		private List<People> _people;
		public List<People> People
		{
			get{return _people;}
			set{
				_people = value;
				RaisePropertyChanged (() => People);
			}
		}

		private int _userID;
		public int UserID
		{ 
			get { return _userID; }
			set { _userID = value; RaisePropertyChanged(() => UserID); }
		}

		private string _userFirstName;
		public string FirstName
		{ 
			get { return _userFirstName; }
			set { _userFirstName = value; RaisePropertyChanged(() => FirstName); }
		}

		private string _userLastName;
		public string LastName
		{ 
			get { return _userLastName; }
			set { _userLastName = value; RaisePropertyChanged(() => LastName); }
		}

		private string _position;
		public string Position {	
			get{ return _position; }
			set {
				_position = value;
				RaisePropertyChanged (() => Position);
			}
		} 

		private string _about;
		public string About{
			get{ return  _about; }
			set{
				_about = value;
				RaisePropertyChanged (() => About);
			}
		}

		private string _email;
		public string Email
		{ 
			get { return _email; }

			set { _email = value; RaisePropertyChanged(() => Email); }
		}

		private DateTime _dateOfBirth;
		public DateTime DateOfBirth
		{ 
			get { return _dateOfBirth.Date; }

			set { _dateOfBirth = value; RaisePropertyChanged(() => DateOfBirth); }
		}

		private string _profileIcon;
		public string ProfileIcon
		{
			get{ return _profileIcon; }
			set{
				_profileIcon = value;
				RaisePropertyChanged (() => ProfileIcon);
			}
		}

		public void UpdateUser(string token)
		{
			GetInformation (token);
			profiledb = new INTADatabase ();
			profiledb.getProfile ();
			profiledb.UpdateProfile (_userFirstName, _userLastName, _email, _dateOfBirth, _profileIcon, _about,  _position, _userID);
		}

		public void GetInformation(string _accessToken){

			var url = "https://www.yammer.com/api/v1/users/current.json";


			HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create (url);
			httpWebRequest.Method = "GET";
			httpWebRequest.Headers ["Authorization"] =
				"Bearer " + _accessToken;
			httpWebRequest.Accept = "application/json";
			httpWebRequest.BeginGetResponse (Response_Completed, httpWebRequest);
			mrse.WaitOne ();
		}

		private void Response_Completed(IAsyncResult result)
		{
			HttpWebRequest request = (HttpWebRequest)result.AsyncState;
			HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);

			using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
			{
				string responseFromServer = streamReader.ReadToEnd();

				JObject user = JObject.Parse (responseFromServer);

				ProfileIcon = user ["mugshot_url"].ToString ();
				UserID = (int)user ["id"];

				if(!String.IsNullOrEmpty(user["first_name"].ToString()))
					FirstName = user["first_name"].ToString();
				if(!String.IsNullOrEmpty(user["last_name"].ToString()))
					LastName = user["last_name"].ToString();
				if(!String.IsNullOrEmpty(user["email"].ToString()))
					Email = user ["email"].ToString();
				if(!String.IsNullOrEmpty(user["birth_date"].ToString()))
					DateOfBirth = Convert.ToDateTime(user ["birth_date"].ToString());

				if (!String.IsNullOrEmpty(user ["job_title"].ToString())) {
					var position = user ["job_title"].ToString();
					if (!String.IsNullOrEmpty(user ["location"].ToString())) {
						var location = user ["location"].ToString ();
						var fullTitle = position + " At " + location;
						Position = fullTitle;
					} else {
						Position = position;
					}
				}

				if (!String.IsNullOrEmpty (user ["interests"].ToString ())) {
					_about = "Interests:" + user ["interests"].ToString ();
				} else {
					_about = "Interests: No Data";
				}

				if (!String.IsNullOrEmpty (user ["summary"].ToString ())) {
					_about += "\n\nAbout:" + user ["summary"].ToString ();
				} else {
					_about += "\n\nAbout: No Data";
				}


			}
			mrse.Set ();
		}


	}
}
