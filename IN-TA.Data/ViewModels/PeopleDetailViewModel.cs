﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Net;
using System.IO;
using INTA;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Net.Http;
using System.Net.Http.Headers;
using INTA.Data;
using System.Threading;

namespace INTA.Data.ViewModels
{
	public class PeopleDetailViewModel:Cirrious.MvvmCross.ViewModels.MvxViewModel
	{
		public ManualResetEvent mrse = new ManualResetEvent(false);
		private INTADatabase profiledb;

		public void saveContact(byte[] _imgArray){
			profiledb = new INTADatabase ();
			var _name = _firstName + " " + _lastName;
			profiledb.saveContact (_peopleUserID, _name, _jobTitle, _email, _imgArray);
		}

		//People Details

		//get people detials from yammer
		public void GetPeopleDetailFromYammer (){
			if(!_peopleUserID.Equals(0)){
				var url = "https://www.yammer.com/api/v1/users/" + _peopleUserID + ".json";
				HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
				httpWebRequest.Method = "GET";
				httpWebRequest.Headers["Authorization"] =
					"Bearer " + _accessToken;
				httpWebRequest.Accept = "application/json";
				httpWebRequest.BeginGetResponse(Response_Completed, httpWebRequest);
				mrse.WaitOne ();
			}
		}

		private void Response_Completed(IAsyncResult result)
		{
			HttpWebRequest request = (HttpWebRequest)result.AsyncState;
			HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);

			using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
			{
				string responseFromServer = streamReader.ReadToEnd();

				JObject peopleDetail = JObject.Parse (responseFromServer);

				if (!string.IsNullOrEmpty (peopleDetail ["full_name"].ToString ())) {
					var fullName = peopleDetail ["full_name"].ToString ();
					var names = fullName.Split (' ');
					_firstName = names [0];
					_lastName = names [1];
				} else {
					_firstName = "No Name";
				}


				if (!string.IsNullOrEmpty (peopleDetail ["job_title"].ToString ())) {
					_jobTitle = peopleDetail ["job_title"].ToString ();
				} else {
					_jobTitle = "No Title";
				}

				_imgUrl = peopleDetail ["mugshot_url"].ToString ();
				_email = peopleDetail ["email"].ToString ();

				if (!string.IsNullOrEmpty (peopleDetail ["birth_date"].ToString ())) {
					_birthday = peopleDetail ["birth_date"].ToString ();
				} else {
					_birthday = "No Data";
				}

				if (!String.IsNullOrEmpty (peopleDetail ["interests"].ToString ())) {
					_about = "Interests:" + peopleDetail ["interests"].ToString ();
				} else {
					_about = "Interests: No Data";
				}

				if (!String.IsNullOrEmpty (peopleDetail ["summary"].ToString ())) {
					_about += "\n\nAbout:" + peopleDetail ["summary"].ToString ();
				} else {
					_about += "\n\nAbout: No Data";
				}

			}
			DoRefresh ();
			mrse.Set ();
		}

		//people details
		private int _peopleUserID;
		public int PeopleUserID
		{ 
			get { return _peopleUserID; }
			set { _peopleUserID = value; RaisePropertyChanged(() => PeopleUserID); }
		}

		private string _firstName;
		public string FirstName
		{ 
			get { return _firstName; }
			set { _firstName = value; RaisePropertyChanged(() => FirstName); }
		}

		private string _lastName;
		public string LastName
		{ 
			get { return _lastName; }
			set { _lastName = value; RaisePropertyChanged(() => LastName); }
		}

		private string _jobTitle;
		public string JobTitle
		{ 
			get { return _jobTitle; }
			set { _jobTitle = value; RaisePropertyChanged(() => JobTitle); }
		}

		private string _imgUrl;
		public string ImgUrl
		{ 
			get { return _imgUrl; }
			set { _imgUrl = value; RaisePropertyChanged(() => ImgUrl); }
		}

		public string getImgUrl(){
			return _imgUrl;
		}

		private string _email;
		public string Email
		{
			get{ return _email; }
			set{
				_email = value;
				RaisePropertyChanged (() => Email);
			}
		}

		private string _birthday;
		public string Birthday
		{
			get{ return "Birthday: " + _birthday; }
			set{
				_birthday = value;
				RaisePropertyChanged (() => Birthday);
			}
		}


		private string _about;
		public string About{
			get{ return _about; }
			set{
				_about = value;
				RaisePropertyChanged (() => About);
			}
		}

		private string _accessToken;
		public string AccessToken{
			get{ return _accessToken; }
			set{
				_accessToken = value;
				RaisePropertyChanged (() => AccessToken);
			}
		}

		public void DoRefresh(){
			RaiseAllPropertiesChanged ();
		}

	}
}
