﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;

namespace INTA.Data.ViewModels
{
	public class FriendsViewModel:Cirrious.MvvmCross.ViewModels.MvxViewModel
	{
		INTADatabase intaDatabase;
		private List<Friends> _friends;
		public List<Friends> Friends
		{ 
			get { return _friends; }
			set { _friends = value; RaisePropertyChanged(() => Friends); }
		}

		//get friends list from database
		public void DoRefreshData(){
			intaDatabase = new INTADatabase ();
			intaDatabase.getFriends ();
			_friends = intaDatabase.getFriendsList ();
		}
			
	}
}

