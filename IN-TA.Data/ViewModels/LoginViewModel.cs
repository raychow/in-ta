﻿using System;
using Cirrious.MvvmCross.ViewModels;
namespace INTA.Data.ViewModels
{
	public class LoginViewModel: Cirrious.MvvmCross.ViewModels.MvxViewModel
	{
		private string _description = "To access all functions in this app, please login with you yammer account.";
		public string Description
		{ 
			get { return _description; }
			set { _description = value; RaisePropertyChanged(() => Description); }
		}

		private string _btnLoginTitle = "Login";

		public string BtnLoginTitle
		{ 
			get { return _btnLoginTitle; }
			set { _btnLoginTitle = value; RaisePropertyChanged(() => BtnLoginTitle); }
		}
	}
}

