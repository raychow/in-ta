using Cirrious.MvvmCross.ViewModels;
using System;


namespace INTA.Data.ViewModels
{
    public class FeedsViewModel 
		: Cirrious.MvvmCross.ViewModels.MvxViewModel
    {
		private string _feeds = "To access all functions in this app, please login with you yammer account.";
		public string Feeds
		{ 
			get { return _feeds; }
			set { _feeds = value; RaisePropertyChanged(() => Feeds); }
		}

		private string _btnLoginTitle = "First Page";

		public string BtnLoginTitle
		{ 
			get { return _btnLoginTitle; }
			set { _btnLoginTitle = value; RaisePropertyChanged(() => BtnLoginTitle); }
		}
    }
}
