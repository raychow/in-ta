﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace INTA.Data.ViewModels
{
	public class AddPeopleToEventViewModel:Cirrious.MvvmCross.ViewModels.MvxViewModel
	{
		INTADatabase intaDatabase;
		public ManualResetEvent mrse = new ManualResetEvent(false);


		private List<Friends> _friends;
		public List<Friends> Friends
		{ 
			get { return _friends; }
			set { _friends = value; RaisePropertyChanged(() => Friends); }
		}

		//get friends list from database
		public void DoRefreshData(){
			intaDatabase = new INTADatabase ();
			intaDatabase.getFriends ();
			_friends = intaDatabase.getFriendsList ();
			intaDatabase.getProfile ();
			getUserContact ();
		}

		//get user basic contact from database
		public void getUserContact(){
			FirstName = intaDatabase.GetFirstName();
			LastName = intaDatabase.GetLastName();
			Email = intaDatabase.GetEmail();
		}
			
		private string _firstName;
		public string FirstName{
			get{ return _firstName; }
			set{
				_firstName = value;
				RaisePropertyChanged (() => FirstName);
			}
		}

		private string _lastName;
		public string LastName{
			get{ return _lastName; }
			set{
				_lastName = value;
				RaisePropertyChanged (() => LastName);
			}
		}

		private string _email;
		public string Email{
			get{ return _email; }
			set{
				_email = value;
				RaisePropertyChanged (() => Email);
			}
		}

	}
}

