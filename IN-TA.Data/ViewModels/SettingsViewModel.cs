﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace INTA.Data.ViewModels
{
	public class SettingsViewModel:Cirrious.MvvmCross.ViewModels.MvxViewModel
	{
		private string _settings = "Settings View Controller";
		public string Settings
		{ 
			get { return _settings; }
			set { _settings = value; RaisePropertyChanged(() => Settings); }
		}
	}
}

