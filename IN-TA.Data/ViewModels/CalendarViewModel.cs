﻿using Cirrious.MvvmCross.ViewModels;
using System;

namespace INTA.Data.ViewModels
{
	public class CalendarViewModel: Cirrious.MvvmCross.ViewModels.MvxViewModel
	{
		private string _calendar = "Events On Today";
		public string Calendar
		{ 
			get { return _calendar; }
			set { _calendar = value; RaisePropertyChanged(() => Calendar); }
		}
	}
}

