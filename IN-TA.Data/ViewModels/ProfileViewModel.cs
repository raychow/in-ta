﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace INTA.Data.ViewModels
{
	public class ProfileViewModel:Cirrious.MvvmCross.ViewModels.MvxViewModel
	{
		//declaration
		public ManualResetEvent mrse = new ManualResetEvent(false);
		private INTADatabase profiledb;
		private string _btnTitleYammer;
		private string _aboutTitle;
		private MvxCommand _saveChange;
		private MvxCommand _cancelChange;


		public IMvxCommand SaveChange {
			get {
				_saveChange = _saveChange ?? new Cirrious.MvvmCross.ViewModels.MvxCommand (DoSaveChange);
				return _saveChange;
			}
		}

		public IMvxCommand CancelChange {
			get {
				_cancelChange = _cancelChange ?? new Cirrious.MvvmCross.ViewModels.MvxCommand (DoRefreshData);
				return _cancelChange;
			}
		}


		public void DoSaveImage(){
			profiledb.UpdateProfileIcon (_profileIcon);
		}

		public void DoSaveChange()
		{
			profiledb.UpdateProfile (_firstName, _lastName, _email, _dateOfBirth, _profileIcon, _about, _position, _userID);
			DoRefreshData ();
		}

		public void DoRefreshData(){
			profiledb = new INTADatabase ();
			profiledb.getProfile ();
			if (!string.IsNullOrEmpty (profiledb.GetAccessToken ())) {
				_firstName = profiledb.GetFirstName ();
				_lastName = profiledb.GetLastName ();
				_email = profiledb.GetEmail ();
				_dateOfBirth = profiledb.GetDateOfBirth ();
				_stringDOB = _dateOfBirth.ToString ("dd MMM");
				_profileIcon = profiledb.GetIcon ();
				_about = profiledb.GetAbout ();
				_position = profiledb.GetPosition ();
				_accessToken = profiledb.GetAccessToken ();
				_userID = profiledb.GetUserID ();
				this.RaiseAllPropertiesChanged ();
			} else {
				_firstName = "";
				_lastName = "";
				_email = "";
				_dateOfBirth = DateTime.Today;
				_stringDOB = _dateOfBirth.ToString ("dd MMM");
				_profileIcon = "";
				_about = "Please login with your yammer account";
				_position = "";
				this.RaiseAllPropertiesChanged ();
			}
		}

		private string _firstName;
		private string _lastName;
		private DateTime _dateOfBirth;
		private string _email;
		private string _stringDOB;
		private string _profileIcon;
		private string _about;
		private string _position;

		//do get profile in constutrcer and using reload to refresh the data

		public ProfileViewModel(){
			_btnTitleYammer = "Yammer";
			_aboutTitle = "About You";
			DoRefreshData ();
		}
		private string _accessToken;
		public string AccessToken{
			get{ return _accessToken; }
			set{
				_accessToken = value;
				RaisePropertyChanged (() => AccessToken);
			}
		}
		public string FirstName
		{ 
			get { return _firstName; }

			set { _firstName = value; RaisePropertyChanged(() => FirstName);}
		}

		public string LastName
		{ 
			get { return _lastName; }

			set { _lastName = value; RaisePropertyChanged(() => LastName); }
		}

		public DateTime DateOfBirth
		{ 
			get { return _dateOfBirth.Date; }

			set { _dateOfBirth = value; RaisePropertyChanged(() => DateOfBirth); }
		}

		public String DateString
		{ 
			get { return _stringDOB; }

			set { _stringDOB = value; RaisePropertyChanged(() => DateOfBirth); }
		}

		public string Email
		{ 
			get { return "Email: " + _email; }

			set { _email = value; RaisePropertyChanged(() => Email); }
		}

		public string ProfileIcon
		{
			get{ return _profileIcon; }
			set{
				_profileIcon = value;
				RaisePropertyChanged (() => ProfileIcon);
			}
		}

		public string YammerBtn
		{ 
			get { return _btnTitleYammer; }

			set { _btnTitleYammer = value; RaisePropertyChanged(() => YammerBtn); }
		}

		public string AboutTitle {	
			get{ return _aboutTitle; }
			set {
				_aboutTitle = value;
				RaisePropertyChanged (() => AboutTitle);
			}
		}
			
		public string Position {	
			get{ return _position; }
			set {
				_position = value;
				RaisePropertyChanged (() => Position);
			}
		} 
			
		public string About{
			get{ return  _about; }
			set{
				_about = value;
				RaisePropertyChanged (() => About);
			}
		}
		private int _userID;
		public int UserID{
			get{ return  _userID; }
			set{
				_userID = value;
				RaisePropertyChanged (() => UserID);
			}
		}
	
		//get user details from yammer and update local database
		public void GetInformation(){
			DoRefreshData ();
		}
	}
}
		