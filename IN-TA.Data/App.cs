using Cirrious.CrossCore.IoC;

namespace INTA.Data
{
	public class App : Cirrious.MvvmCross.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
				
            RegisterAppStart<ViewModels.FeedsViewModel>();
        }

    }
}