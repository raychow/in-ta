﻿using System;
using Cirrious.MvvmCross.Plugins.Sqlite;
using Cirrious.CrossCore;
using System.IO;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace INTA.Data
{
	public class INTADatabase
	{
		//Objects declaration 
		private ISQLiteConnectionFactory factory;
		private ISQLiteConnection connection;
		private int profileID;
		private string firstName;
		private string lastName;
		private DateTime dateOfBirth;
		private string email;
		private string profileIcon;
		private string position;
		private string about;
		private string accessToken;
		private int userid;
		private List<Friends> _friends;

		//private string dbPath;
		public INTADatabase ()
		{
			//stilling test
			factory = Mvx.Resolve<ISQLiteConnectionFactory>();
			// open or create the database
			connection = factory.Create("inta.sql");
		}


		public void saveEvent(bool _private, string _eName,string _description, DateTime _startTime, DateTime _endTime, string _location, double _lon, double _lat, int _userID){
			connection.CreateTable<Event> ();
			var newEvent = new Event {
				Private = _private,
				EventName = _eName,
				Description = _description,
				Start = _startTime,
				End = _endTime,
				Location = _location,
				Lon = _lon,
				Lat = _lat,
				Owner = _userID,
				Joined = null
			};
			connection.Insert (newEvent);
			connection.Close ();
		}


		public void getFriends (){
			var friendsList = connection.Table<Friends> ();
			_friends = new List<Friends>();
			_friends.Clear();
			foreach (var f in friendsList) {
				_friends.Add (f);
			}
		}

		public List<Friends> getFriendsList(){
			return _friends;
		}

		public void saveContact(int userID, string fullName, string jobTitle, string email, byte[] imgArray){

			connection.CreateTable<Friends> ();
			var friend = new Friends {
				UserId = userID,
				FullName = fullName,
				JobTitle = jobTitle,
				Email = email,
				ImgUrl = imgArray
			};
			connection.Insert (friend);
			connection.Close ();
		}


		public void getProfile(){
			var userProfile = connection.Table <Profile>();
			foreach (var p in userProfile) {
				profileID = p.ID;
				userid = p.userid;
				firstName = p.FirstName;
				lastName = p.LastName;
				email = p.Email;
				dateOfBirth = p.DateOfBirth;
				profileIcon = p.ProfileIcon;
				about = p.About;
				accessToken = p.AccessToken;
				position = p.Position;
			}
		}

		public string GetFirstName(){
			return firstName;
		}

		public void UpdateProfile(string fName, string lName, string email, DateTime doBirth, string icon, string about, string posit, int userID){

			var person = connection.Get<Profile> (profileID);
			person.FirstName = fName;
			person.LastName = lName;
			person.Email = email;
			person.DateOfBirth = doBirth;
			person.ProfileIcon = icon;
			person.About = about;
			person.Position = posit;
			person.userid = userID;
			connection.Update(person);
		}

		public void UpdateProfileIcon(string icon){
			var person = connection.Get<Profile> (profileID);
			person.ProfileIcon = icon;
			connection.Update (person);
		}

		public void setAccessToken(string Token){
			var person = connection.Get<Profile> (profileID);
			person.AccessToken = Token;
			connection.Update (person);
		}

		public string GetLastName(){
			return lastName;
		}

		public string GetEmail(){
			return email;
		}

		public DateTime GetDateOfBirth(){
			return dateOfBirth;
		}

		public string GetIcon(){
			return profileIcon;
		}

		public string GetPosition(){
			return position;
		}

		public string GetAbout(){
			return about;
		}

		public string GetAccessToken(){
			return accessToken;
		}

		public int GetUserID(){
			return userid;
		}
	}
}

